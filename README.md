# ウェブサイトのエラーを検出し、メール通知するプログラム

活用事例)
- ウェブサイトが動いているか監視する
- 別プログラムが稼働しているか検証する
- 競合ウェブサイトが変更されたら通知する


```go
// gmailの場合は2段階認証を解除し
// Google設定「安全性の低いアプリの許可」を有効にする必要があり
func gmailSend(m mail) error {
	smtpSvr := "smtp.gmail.com:587"
	auth := smtp.PlainAuth("", m.username, m.password, "smtp.gmail.com")
	if err := smtp.SendMail(smtpSvr, auth, m.from, []string{m.to}, []byte(m.body())); err != nil {
		return err
	}
	return nil
}

// メイン関数
func main() {
	var err error

  // エラーを感知するために監視するスケジュールを決める
  // 今回は1時間に一度プログラムを走らせる
  h1 := time.NewTicker(time.Hour * 1)

	// 無限ループで1時間に一度チェックしに行く
	// forといっても処理がないので軽いよ
	for {

		// switchだと動かないよ
		select {

    // 1時間一度通知を受け走らせる
		case <-h1.C:

      // 検出関数
			err = d.Check()

			// エラーを検出したらメールを送る
			if err != nil {
        // 送信用のデータを構造体に入れる
				m := mail{
					// 通知送信用メアドから送信する
					from:     "送信用メールアドレス@gmail.com",
					username: "送信用メールアドレスアカウント名@gmail.com",
					password: "上記メアド認証用パスワード",
					to:       "送信先",
					sub:      "メールタイトル: xxxがクラッシュ",
					msg:      "本文: \nで改行",
				}

        // 上記構造体からデータを受け送信する
				// gmailから対象メアドに送っちゃう
				if err := gmailSend(m); err != nil {
          // エラーだしたら止めてあげる
					log.Println(err)
					os.Exit(1)
				}
			} else {
				log.Println("成功")
			}
		}
	}

  // プログラムが終わっても止まらないとメモリを消費するので止めてあげる
	defer h1.Stop()
}
```
